import {ListOfVideos} from "./ListOfVideos";
import {NewVideo} from "./NewVideo";

const App = () => (
    <div>
        <ListOfVideos />
        <NewVideo />
    </div>
);

export {
    App,
};
