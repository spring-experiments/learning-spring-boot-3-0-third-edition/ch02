import {useState} from "react";

const NewVideo = () => {
    const [name, setName] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        await fetch('api/videos', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({name: name}),
        }).then(() => window.location.href = '/react');
    };

    const handleChange = (event) => {
        setName(event.target.value);
    };

    return (
        <form
            onSubmit={handleSubmit}
        >
            <input
                type="text"
                value={name}
                onChange={handleChange}
            />
            <button type="submit">Submit</button>
        </form>
    );
};

export {
    NewVideo,
};
