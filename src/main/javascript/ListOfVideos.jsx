import {useEffect, useState} from "react";

const ListOfVideos = () => {
    const [videos, setVideos] = useState([]);

    useEffect(() => {
        const loadData = async () => {
            const json = await (await fetch('/api/videos')).json();
            setVideos(json);
        };
        loadData()
            .catch(err => console.error(err));
    }, []);

    return (
        <ul>
            {videos.map(item => (
                <li>
                    {item.name}
                </li>
            ))}
        </ul>
    );
};

export {
    ListOfVideos,
};
