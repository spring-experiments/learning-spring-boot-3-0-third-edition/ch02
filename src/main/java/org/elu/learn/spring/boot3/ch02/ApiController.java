package org.elu.learn.spring.boot3.ch02;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/videos")
public class ApiController {
    private final VideoService videoService;

    public ApiController(VideoService videoService) {
        this.videoService = videoService;
    }

    @GetMapping
    public List<Video> all() {
        return videoService.getVideos();
    }

    @PostMapping
    public Video newVideo(@RequestBody Video newVideo) {
        return videoService.create(newVideo);
    }
}
